const Vue = require('vue/dist/vue.min.js');
const VuePersist = require("vue-persist");

var modalComponent = {
  template: '#modal-template',
  methods: {
    'closeModal': function () {
      this.$emit('close');
    }
  } 
};

Vue.use(VuePersist, {
  name: 'store',
  expiration: 0
});

var app = new Vue({
  el: '#app',
  components: {
    'modal': modalComponent
  },
  data: {
    showProjectsModal: false, 
    showCardsModal: false, 
    
    tr: 20,
    
    meProduction: 1,
    meValue: 42,
    
    steelProduction: 1,
    steelValue: 0,
    
    titaniumProduction: 1,
    titaniumValue: 0,
    
    plantsProduction: 1,
    plantsValue: 0,
    
    energyProduction: 1,
    energyValue: 0,
    
    heatProduction: 1,
    heatValue: 0,
    
    generationCounter: 1
  },
  persist: [ 'tr', 'meProduction', 'meValue', 'steelProduction',
    'steelValue', 'titaniumProduction', 'titaniumValue', 'plantsProduction',
    'plantsValue', 'energyProduction', 'energyValue', 'heatProduction',
    'heatValue', 'generationCounter' ],
  methods: {
    productionPhase: function () {
      // Transform Energy into Heat
      this.heatValue += this.energyValue;
      this.energyValue = 0;
      
      // Earn ME
      this.meValue += this.tr + this.meProduction;
      
      // Production
      this.steelValue += this.steelProduction;
      this.titaniumValue += this.titaniumProduction;
      this.plantsValue += this.plantsProduction;
      this.energyValue += this.energyProduction;
      this.heatValue += this.heatProduction;
      
      this.generationCounter += 1;
    },
    
    handlePowerPlantButtonClick: function () {
      this.meValue -= 11;
      this.$refs.projectsModal.closeModal();
    },
    
    handleAsteroidButtonClick: function () {
      this.meValue -= 14;
      this.$refs.projectsModal.closeModal();
    },
    
    handleAquifierButtonClick: function () {
      this.meValue -= 18;
      this.$refs.projectsModal.closeModal();
    },
    
    handleGreeneryButtonClick: function () {
      this.meValue -= 23;
      this.$refs.projectsModal.closeModal();
    },
    
    handleCityButtonClick: function () {
      this.meValue -= 25;
      this.meProduction += 1;
      this.$refs.projectsModal.closeModal();
    },
    
    handleCancelButtonClick: function () {
      this.$refs.projectsModal.closeModal();
    },
    
    // Cards Modal
    handleCardsModal1Card: function () {
      this.meValue -= 3;
      this.$refs.cardsModal.closeModal();
    },
    
    handleCardsModal2Cards: function () {
      this.meValue -= 6;
      this.$refs.cardsModal.closeModal();
    },
    
    handleCardsModal3Cards: function () {
      this.meValue -= 9;
      this.$refs.cardsModal.closeModal();
    },
    
    handleCardsModal4Cards: function () {
      this.meValue -= 12;
      this.$refs.cardsModal.closeModal();
    },
    
    handleCardsModalCancelButtonClick: function () {
      this.$refs.cardsModal.closeModal();
    },
    
    handleNewGameClick: function () {
      this.tr = 20;

      this.meProduction = 1;
      this.meValue = 42;

      this.steelProduction = 1;
      this.steelValue = 0;

      this.titaniumProduction = 1;
      this.titaniumValue = 0;

      this.plantsProduction = 1;
      this.plantsValue = 0;

      this.energyProduction = 1;
      this.energyValue = 0;

      this.heatProduction = 1;
      this.heatValue = 0;

      this.generationCounter = 1;
      
      document.getElementById("menuToggleInput").checked = false;
    },
  }
});
