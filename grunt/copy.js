module.exports = {
  all: {
    files: [
      // .html files
      {
        expand: true, 
        cwd: 'src',
        src: ['*.html'], 
        dest: 'dist/', 
        filter: 'isFile'
      },

      // Font files
      {
        expand: true, 
        cwd: 'src',
        src: ['font/*'], 
        dest: 'dist/', 
        filter: 'isFile'
      },
      
      // Images
      {
        expand: true,
        cwd: 'build', 
        src: ['img/*'], 
        dest: 'dist/', 
        filter: 'isFile'
      },
      
      // Javascript (niepotrzebny jeśli uglify zadziała)
      {
        expand: true,
        cwd: 'build', 
        src: ['js/*'], 
        dest: 'dist/', 
        filter: 'isFile'
      },
    ],
  },
};
