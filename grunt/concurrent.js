module.exports = {

    // Task options
    options: {
        limit: 3
    },

    // Dev tasks
    devFirst: [
        'clean',
        //'jshint'
    ],
    devSecond: [
        'compass:dev',
        'imagemin:dev'
    ],
    devThird: [
        'copy:dev'
    ],

    // Production tasks
    prodFirst: [
        'clean',
        'jshint'
    ],
    prodSecond: [
        'compass:prod',
        'imagemin:prod',
        'uglify:prod'
    ],
    prodThird: [
        'copy:prod'
    ],

};
