module.exports = {
  all: {
    files: [{
      expand: true,
      cwd: 'build/js',
      src: '*.js',
      dest: 'dist/js',
      ext: '.min.js',
      // debug: 'true' 
    }]
  },
};
