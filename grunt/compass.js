module.exports = {
    all: {
        options: {
          outputStyle: 'expanded',
          sassDir: 'src/scss',
          cssDir: 'build/css',
        }
    },
};
