module.exports = {
  all: {
    files: [{
      expand: true,
      cwd: 'build/css',
      src: ['*.css', '!*.min.css'],
      dest: 'dist/css',
      ext: '.min.css'
    }]
  }
};